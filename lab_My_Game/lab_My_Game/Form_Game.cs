﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab_My_Game
{
    public partial class Form_Game : Form
    {
        DateTime date1 = new DateTime(0, 0);
        Game game;
        int shag = 0;
        public Form_Game()
        {
            InitializeComponent();
            if (timer1.Enabled == true)
                timer1.Enabled = false;
            else
                timer1.Enabled = true;
            timer1.Tick += Timer1_Tick;
            timer1.Interval = 1000;
            game = new Game(4);
            this.KeyDown += Form_Game_KeyDown;
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            date1 = date1.AddSeconds(1);
            laTime.Text = date1.ToString("mm:ss");
        }

        private void Form_Game_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    if (e.Shift)
                        for (int i = 0; i < 3; i++)
                            game.new_shift(0);
                    else
                        game.new_shift(0);
                    break;
                case Keys.Up:
                    if (e.Shift)
                        for (int i = 0; i < 3; i++)
                            game.new_shift(2);
                    else
                        game.new_shift(2);
                    break;
                case Keys.Down:
                    if (e.Shift)
                        for (int i = 0; i < 3; i++)
                            game.new_shift(3);
                    else
                        game.new_shift(3);
                    break;
                case Keys.Right:
                    if (e.Shift)
                        for (int i = 0; i < 3; i++)
                            game.new_shift(1);
                    else
                        game.new_shift(1);
                    break;
            }
            refresh();
            if (game.check_num())
            {
                MessageBox.Show("Поздравляем! Ты выйграл за " + date1.ToString("mm: ss") + " секунд", "Ты победил");
                start();
            }
        }
        private void check_movie()
        {
            shag++;
            la_Check.Text = shag.ToString();
        }

        private void button0_Click(object sender, EventArgs e)
        {
            int button_tag = Convert.ToInt32(((Button)sender).Tag);
            game.shift(button_tag);
            refresh();
            if (game.check_num())
                MessageBox.Show("Поздравляем!","Ты победил");

        }

        private Button button (int position)
        {
            switch (position)
            {
                case 0: return button0;
                case 1: return button1;
                case 2: return button2;
                case 3: return button3;
                case 4: return button4;
                case 5: return button5;
                case 6: return button6;
                case 7: return button7;
                case 8: return button8;
                case 9: return button9;
                case 10: return button10;
                case 11: return button11;
                case 12: return button12;
                case 13: return button13;
                case 14: return button14;
                case 15: return button15;
                default: return null;
            }
        }

        private void start(int i = 100)
        {
            timer1.Start();
            int max = i;
            game.start_game();
            for (int j =0; j < max; j++)
            {
                game.shift_random();
            }
            refresh();
        }
        private void refresh()
        {
            for ( int button_tag = 0; button_tag < 16; button_tag++)
            {
                int num = game.get_number(button_tag);
                button(button_tag).Text = num.ToString();
                button(button_tag).Visible = (num > 0); 
            }
            check_movie();
        }

        private void Form_Game_Load(object sender, EventArgs e)
        {
            start();
        }

        private void menu_new_refresh(object sender, EventArgs e)
        {
            game.start_game();
            refresh();
        }

        private void simple_start_form_click(object sender, EventArgs e)
        {
            start(50);
        }

        private void hard_start_form_Click(object sender, EventArgs e)
        {
            start(100);
        }

        private void super_hard_start_form_Click(object sender, EventArgs e)
        {
            start(150);
        }

        private void new_game_type_click(object sender, EventArgs e)
        {
            Hide();
            Form2 new_form = new Form2();
            new_form.ShowDialog();
            Close();
            
        }
    }
}
