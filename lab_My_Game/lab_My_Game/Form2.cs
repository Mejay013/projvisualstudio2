﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab_My_Game
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
            Form1 game_3 = new Form1();
            game_3.ShowDialog();
            Close();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Hide();
            Form_Game game_4 = new Form_Game();
            game_4.ShowDialog();
            Close();
        }
    }
}
