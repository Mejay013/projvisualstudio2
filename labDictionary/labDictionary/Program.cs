﻿using System;
using System.Collections.Generic;

namespace labDictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, string> x = new Dictionary<int, string>(5);
            x.Add(1, "Bryansk");
            x.Add(2, "Tyla");
            x.Add(3, "Samara");
            x.Add(4, "Kaluga");
            x.Add(5, "Pskov");

            foreach ( KeyValuePair<int,string> keyValue in x)
            {
                Console.WriteLine(keyValue.Key + " - " + keyValue.Value);
            }

            string country = x[4];
            x[4] = "Spain";
            x.Remove(2);

        }
    }
}
