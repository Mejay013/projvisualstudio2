﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebBrowser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            buGo.Click += BuGo_Click;
            buBack.Click += BuBack_Click;
            buStop.Click += BuStop_Click;
            buReload.Click += BuReload_Click;
            buNext.Click += BuNext_Click;
        }

        private void BuNext_Click(object sender, EventArgs e)
        {
            webBrowser1.GoForward();
        }

        private void BuReload_Click(object sender, EventArgs e)
        {
            webBrowser1.Refresh();
        }

        private void BuStop_Click(object sender, EventArgs e)
        {
            webBrowser1.Stop();
        }

        private void BuBack_Click(object sender, EventArgs e)
        {
            webBrowser1.GoBack();
        }

        private void BuGo_Click(object sender, EventArgs e)
        {
            webBrowser1.Navigate("https://" + textBox1.Text.ToString());
        }
    }
}
