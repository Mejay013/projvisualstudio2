﻿namespace WebBrowser
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buGo = new System.Windows.Forms.Button();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.buBack = new System.Windows.Forms.Button();
            this.buNext = new System.Windows.Forms.Button();
            this.buReload = new System.Windows.Forms.Button();
            this.buStop = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(145, 44);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(335, 20);
            this.textBox1.TabIndex = 0;
            // 
            // buGo
            // 
            this.buGo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buGo.Location = new System.Drawing.Point(486, 44);
            this.buGo.Name = "buGo";
            this.buGo.Size = new System.Drawing.Size(83, 23);
            this.buGo.TabIndex = 1;
            this.buGo.Text = "Go";
            this.buGo.UseVisualStyleBackColor = true;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser1.Location = new System.Drawing.Point(52, 70);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(459, 226);
            this.webBrowser1.TabIndex = 2;
            // 
            // buBack
            // 
            this.buBack.Location = new System.Drawing.Point(6, 40);
            this.buBack.Name = "buBack";
            this.buBack.Size = new System.Drawing.Size(27, 23);
            this.buBack.TabIndex = 3;
            this.buBack.Text = "<";
            this.buBack.UseVisualStyleBackColor = true;
            // 
            // buNext
            // 
            this.buNext.Location = new System.Drawing.Point(31, 40);
            this.buNext.Name = "buNext";
            this.buNext.Size = new System.Drawing.Size(27, 23);
            this.buNext.TabIndex = 4;
            this.buNext.Text = ">";
            this.buNext.UseVisualStyleBackColor = true;
            // 
            // buReload
            // 
            this.buReload.Location = new System.Drawing.Point(64, 40);
            this.buReload.Name = "buReload";
            this.buReload.Size = new System.Drawing.Size(75, 23);
            this.buReload.TabIndex = 5;
            this.buReload.Text = "Reload";
            this.buReload.UseVisualStyleBackColor = true;
            // 
            // buStop
            // 
            this.buStop.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buStop.Location = new System.Drawing.Point(243, 301);
            this.buStop.Name = "buStop";
            this.buStop.Size = new System.Drawing.Size(75, 23);
            this.buStop.TabIndex = 6;
            this.buStop.Text = "Stop";
            this.buStop.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 336);
            this.Controls.Add(this.buStop);
            this.Controls.Add(this.buReload);
            this.Controls.Add(this.buNext);
            this.Controls.Add(this.buBack);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.buGo);
            this.Controls.Add(this.textBox1);
            this.MinimumSize = new System.Drawing.Size(478, 347);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buGo;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Button buBack;
        private System.Windows.Forms.Button buNext;
        private System.Windows.Forms.Button buReload;
        private System.Windows.Forms.Button buStop;
    }
}

