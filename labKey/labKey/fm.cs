﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labKey
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();

            this.KeyDown += Fm_KeyDown;
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            laName.Text = e.Shift ? "Shift " + e.KeyCode.ToString() : e.KeyCode.ToString();
           
        }
    }
}
