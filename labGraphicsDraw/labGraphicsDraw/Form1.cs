﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labGraphicsDraw
{
    public partial class Form1 : Form
    {
        private Bitmap b;
        private Graphics g;
        private Point startpoint;
        private Pen myPen;

        public Form1()
        {
            InitializeComponent();
            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            myPen = new Pen(Color.Red, 10);
            myPen.StartCap = myPen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            

            pictureBox1.MouseDown += (s, e) => startpoint = e.Location;
            pictureBox1.MouseMove += PictureBox1_MouseMove;
            pictureBox1.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);

            pictureBox2.Click += (s, e) => myPen.Color = pictureBox2.BackColor;
            pictureBox3.Click += (s, e) => myPen.Color = pictureBox3.BackColor;
            pictureBox4.Click += (s, e) => myPen.Color = pictureBox4.BackColor;
            pictureBox5.Click += (s, e) => myPen.Color = pictureBox5.BackColor;

            trackBar1.Value = Convert.ToInt32(myPen.Width);
            trackBar1.ValueChanged += (s, e) => myPen.Width = trackBar1.Value;

            button1.Click += (s, e) => { g.Clear(DefaultBackColor); pictureBox1.Invalidate(); };
        }

        private void PictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                g.DrawLine(myPen, startpoint, e.Location);
                startpoint = e.Location;
                pictureBox1.Invalidate();
            }
        }
    }
}
