﻿namespace labSQlite
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvLogs = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.edSQL = new System.Windows.Forms.TextBox();
            this.buUsersShow = new System.Windows.Forms.Button();
            this.buNotesShow = new System.Windows.Forms.Button();
            this.buRunOne = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.edNotesPriority = new System.Windows.Forms.NumericUpDown();
            this.buNotesAdd = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edNotesPriority)).BeginInit();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.lvLogs.HideSelection = false;
            this.lvLogs.Location = new System.Drawing.Point(32, 46);
            this.lvLogs.Name = "listView1";
            this.lvLogs.Size = new System.Drawing.Size(402, 799);
            this.lvLogs.TabIndex = 0;
            this.lvLogs.UseCompatibleStateImageBehavior = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(278, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = " Лог запусков приложения";
            // 
            // textBox1
            // 
            this.edSQL.Location = new System.Drawing.Point(473, 46);
            this.edSQL.Multiline = true;
            this.edSQL.Name = "textBox1";
            this.edSQL.Size = new System.Drawing.Size(1117, 166);
            this.edSQL.TabIndex = 2;
            // 
            // button1
            // 
            this.buUsersShow.Location = new System.Drawing.Point(473, 234);
            this.buUsersShow.Name = "button1";
            this.buUsersShow.Size = new System.Drawing.Size(212, 42);
            this.buUsersShow.TabIndex = 3;
            this.buUsersShow.Text = "Пользователи";
            this.buUsersShow.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.buNotesShow.Location = new System.Drawing.Point(701, 234);
            this.buNotesShow.Name = "button2";
            this.buNotesShow.Size = new System.Drawing.Size(203, 43);
            this.buNotesShow.TabIndex = 4;
            this.buNotesShow.Text = "Заметки";
            this.buNotesShow.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.buRunOne.Location = new System.Drawing.Point(1116, 235);
            this.buRunOne.Name = "button3";
            this.buRunOne.Size = new System.Drawing.Size(195, 42);
            this.buRunOne.TabIndex = 5;
            this.buRunOne.Text = "Выполинить 1";
            this.buRunOne.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1338, 235);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(205, 42);
            this.button4.TabIndex = 6;
            this.button4.Text = "Выполнить SQL";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(473, 296);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 82;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(1077, 447);
            this.dataGridView1.TabIndex = 7;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(473, 784);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(469, 40);
            this.textBox2.TabIndex = 8;
            // 
            // numericUpDown1
            // 
            this.edNotesPriority.Location = new System.Drawing.Point(971, 785);
            this.edNotesPriority.Name = "numericUpDown1";
            this.edNotesPriority.Size = new System.Drawing.Size(156, 31);
            this.edNotesPriority.TabIndex = 9;
            // 
            // button5
            // 
            this.buNotesAdd.Location = new System.Drawing.Point(1282, 784);
            this.buNotesAdd.Name = "button5";
            this.buNotesAdd.Size = new System.Drawing.Size(268, 39);
            this.buNotesAdd.TabIndex = 10;
            this.buNotesAdd.Text = "Добавить заметку";
            this.buNotesAdd.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(477, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 25);
            this.label2.TabIndex = 11;
            this.label2.Text = "SQL";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1602, 868);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buNotesAdd);
            this.Controls.Add(this.edNotesPriority);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.buRunOne);
            this.Controls.Add(this.buNotesShow);
            this.Controls.Add(this.buUsersShow);
            this.Controls.Add(this.edSQL);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvLogs);
            this.MaximumSize = new System.Drawing.Size(1628, 939);
            this.MinimumSize = new System.Drawing.Size(1628, 939);
            this.Name = "Form1";
            this.Text = "labSqlite";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edNotesPriority)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvLogs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edSQL;
        private System.Windows.Forms.Button buUsersShow;
        private System.Windows.Forms.Button buNotesShow;
        private System.Windows.Forms.Button buRunOne;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.NumericUpDown edNotesPriority;
        private System.Windows.Forms.Button buNotesAdd;
        private System.Windows.Forms.Label label2;
    }
}

