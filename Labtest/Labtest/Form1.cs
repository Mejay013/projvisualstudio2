﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Labtest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            textBox1.Text = Directory.GetCurrentDirectory();
            button1.Click += (s, e) => webBrowser1.GoBack();
            button2.Click += (s, e) => webBrowser1.GoForward();
            button3.Click += (s, e) => webBrowser1.Url = new Uri(Directory.GetParent(textBox1.Text).ToString());
            button4.Click += Button4_Click;
            webBrowser1.DocumentCompleted += (s, e) => textBox1.Text = webBrowser1.Url.LocalPath;
            webBrowser1.Url = new Uri(textBox1.Text);


        }

        private void Button4_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if(dialog.ShowDialog() == DialogResult.OK)
            {
                webBrowser1.Url = new Uri(dialog.SelectedPath);
            }
        }
    }
}
