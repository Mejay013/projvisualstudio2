﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labBufControlOnMouse
{
    public partial class fm : Form
    {
        private Point StartPoint { get; set; }
        public fm()
        {
            InitializeComponent();
            panel1.MouseDown += AllMouseDown;
            panel1.MouseMove += AllMouseMove;
            panel2.MouseDown += AllMouseDown;
            panel2.MouseMove += AllMouseMove;
            pictureBox1.MouseDown += AllMouseDown;
            pictureBox1.MouseMove += AllMouseMove;
        }

        private void AllMouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point x = new Point(Cursor.Position.X - StartPoint.X, Cursor.Position.Y - StartPoint.Y);
                if (sender is Control)
                    ((Control)sender).Location = PointToClient(x);
            }
        }

        private void AllMouseDown(object sender, MouseEventArgs e)
        {
            StartPoint = new Point(e.X, e.Y);
        }

        private void fm_Load(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            
        }
    }
}
