﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace LabPersonalProject
{
    class Bullet
    {
        public string directions;
        public int bulletLeft;
        public int bulletTop;

        private int speed = 20;
        private PictureBox bullet = new PictureBox();
        private Timer bulletTimer = new Timer();



        public void MakeBullet(Form form)
        {
            bullet.BackColor = Color.White;
            bullet.Size = new Size(5, 5);
            bullet.Tag = "bullet";
            bullet.Left = bulletLeft;
            bullet.Top = bulletTop;
            bullet.BringToFront();


            form.Controls.Add(bullet);

            bulletTimer.Interval = speed;
            bulletTimer.Tick += new EventHandler(BulletTimerEvent);
            bulletTimer.Start();
        }

        private void BulletTimerEvent(object sender, EventArgs e)
        {

            if (directions == "Left")
            {
                bullet.Left -= speed;
            }
            if (directions == "Right")
            {
                bullet.Left += speed;
            }
            if (directions == "Up")
            {
                bullet.Top -= speed;
            }
            if (directions == "Down")
            {
                bullet.Top += speed;
            }

            if (bulletLeft < 10 || bulletLeft > 1000  || bullet.Top < 10 || bullet.Top > 1700)
            {
                bulletTimer.Stop();
                bulletTimer.Dispose();
                bullet.Dispose();
                bulletTimer = null;
                bullet = null;
            }
        }
    }
}
