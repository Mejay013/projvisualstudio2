﻿namespace LabPersonalProject
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.laAmmo = new System.Windows.Forms.Label();
            this.laScore = new System.Windows.Forms.Label();
            this.laHealth = new System.Windows.Forms.Label();
            this.healthBar = new System.Windows.Forms.ProgressBar();
            this.player = new System.Windows.Forms.PictureBox();
            this.GameTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.player)).BeginInit();
            this.SuspendLayout();
            // 
            // laAmmo
            // 
            this.laAmmo.AutoSize = true;
            this.laAmmo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laAmmo.ForeColor = System.Drawing.Color.White;
            this.laAmmo.Location = new System.Drawing.Point(12, 9);
            this.laAmmo.Name = "laAmmo";
            this.laAmmo.Size = new System.Drawing.Size(164, 39);
            this.laAmmo.TabIndex = 0;
            this.laAmmo.Text = "Ammo: 0";
            // 
            // laScore
            // 
            this.laScore.AutoSize = true;
            this.laScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laScore.ForeColor = System.Drawing.Color.White;
            this.laScore.Location = new System.Drawing.Point(676, 9);
            this.laScore.Name = "laScore";
            this.laScore.Size = new System.Drawing.Size(128, 39);
            this.laScore.TabIndex = 1;
            this.laScore.Text = "Kills: 0";
            // 
            // laHealth
            // 
            this.laHealth.AutoSize = true;
            this.laHealth.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laHealth.ForeColor = System.Drawing.Color.White;
            this.laHealth.Location = new System.Drawing.Point(1186, 9);
            this.laHealth.Name = "laHealth";
            this.laHealth.Size = new System.Drawing.Size(134, 39);
            this.laHealth.TabIndex = 2;
            this.laHealth.Text = "Health:";
            // 
            // healthBar
            // 
            this.healthBar.Location = new System.Drawing.Point(1326, 9);
            this.healthBar.Name = "healthBar";
            this.healthBar.Size = new System.Drawing.Size(323, 39);
            this.healthBar.TabIndex = 3;
            this.healthBar.Value = 100;
            // 
            // player
            // 
            this.player.Image = global::LabPersonalProject.Properties.Resources.up;
            this.player.Location = new System.Drawing.Point(702, 405);
            this.player.Name = "player";
            this.player.Size = new System.Drawing.Size(71, 100);
            this.player.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.player.TabIndex = 4;
            this.player.TabStop = false;
            // 
            // GameTimer
            // 
            this.GameTimer.Enabled = true;
            this.GameTimer.Interval = 20;
            this.GameTimer.Tick += new System.EventHandler(this.MainTimer);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1679, 961);
            this.Controls.Add(this.player);
            this.Controls.Add(this.healthBar);
            this.Controls.Add(this.laHealth);
            this.Controls.Add(this.laScore);
            this.Controls.Add(this.laAmmo);
            this.MaximumSize = new System.Drawing.Size(1705, 1032);
            this.MinimumSize = new System.Drawing.Size(1705, 1032);
            this.Name = "Form1";
            this.Text = "Zombie Shooter";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyIsDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyIsUp);
            ((System.ComponentModel.ISupportInitialize)(this.player)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label laAmmo;
        private System.Windows.Forms.Label laScore;
        private System.Windows.Forms.Label laHealth;
        private System.Windows.Forms.ProgressBar healthBar;
        private System.Windows.Forms.PictureBox player;
        private System.Windows.Forms.Timer GameTimer;
    }
}

