﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using Microsoft.VisualBasic.Devices;

namespace LabPersonalProject
{
    public partial class Form1 : Form
    {
        public bool music;

        bool goLeft, goRight, goUp, goDown, gameOver;
        string facing = "up";
        int playerHealth = 100;
        int speed = 10;
        int ammo = 10;
        int zombiesSpeed = 3;
        int score = 0; 
        Random rand = new Random();

        List<PictureBox> zombiesList = new List<PictureBox>();

        public Form1()
        {
            if (Class1.muscik_check == true)
            {
                music = true;
            }
            else
            {
                music = false;
            }
            

            InitializeComponent();
            
            RestartGame();

        }

        private void MainTimer(object sender, EventArgs e)
        {
            if (playerHealth > 1)
            {
                healthBar.Value = playerHealth;
            }
            else
            {
                gameOver = true;
                player.Image = Properties.Resources.dead;
                
                GameTimer.Stop();
                playSimpleSound2(false);
                var result = MessageBox.Show($"Твой результат: {score.ToString()} \n  Повторить?", "Ты проиграл", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    RestartGame();
                }
                else
                { 
                    this.Close();
                }
            }

            laAmmo.Text = "Ammo: " + ammo;
            laScore.Text = "Kills: " + score;

            if (goLeft == true && player.Left > 0)
            {
                player.Left -= speed;
                this.BackColor = Color.DimGray;
            }
            if (goRight == true && player.Left + player.Width < this.ClientSize.Width)
            {
                player.Left += speed;
                this.BackColor = Color.DimGray;
            }
            if (goUp == true && player.Top > 51)
            {
                player.Top -= speed;
                this.BackColor = Color.DimGray;
            }
            if (goDown == true && player.Top + player.Height < this.ClientSize.Height)
            {
                player.Top += speed;
                this.BackColor = Color.DimGray;
            }


            foreach (Control x in Controls)
            {
                if (x is PictureBox && (string)x.Tag == "ammo")
                {
                    if (player.Bounds.IntersectsWith(x.Bounds))
                    {
                        this.Controls.Remove(x);
                        ((PictureBox)x).Dispose();
                        ammo += 5;
                    }
                }


                if (x is PictureBox && (string)x.Tag == "zombie")
                {


                    if (player.Bounds.IntersectsWith(x.Bounds))
                    {
                        playerHealth--;
                        this.BackColor = Color.Red;
                        

                    }   

                    if (x.Left > player.Left)
                    {
                        x.Left -= zombiesSpeed;
                        
                        ((PictureBox)x).Image = Properties.Resources.zleft;
                    }
                    if (x.Left < player.Left)
                    {
                        x.Left += zombiesSpeed;
                        ((PictureBox)x).Image = Properties.Resources.zright;
                    }
                    if (x.Top > player.Top)
                    {
                        x.Top -= zombiesSpeed;
                        ((PictureBox)x).Image = Properties.Resources.zup;
                    }
                    if (x.Top < player.Top)
                    {
                        x.Top += zombiesSpeed;
                        ((PictureBox)x).Image = Properties.Resources.zdown;
                    }
                }

                foreach (Control j in this.Controls)
                {
                    if (j is PictureBox && (string)j.Tag == "bullet" && x is PictureBox && (string)x.Tag == "zombie")
                    {
                        if (x.Bounds.IntersectsWith(j.Bounds))
                        {
                            score++;

                            this.Controls.Remove(j);
                            ((PictureBox)j).Dispose();
                            this.Controls.Remove(x);
                            ((PictureBox)x).Dispose();

                            zombiesList.Remove((PictureBox)x);
                            MakeZombies();
                        }
                    }
                }

            }

        }
        private void playSimpleSound2(bool check)
        {

            SoundPlayer game_music = new SoundPlayer();
            game_music.SoundLocation = "Z:/Study/Mobile/projvisualstudio2/LabPersonalProject/LabPersonalProject/music/game.wav";
            if (check == true)
            {
                game_music.Play();
            }
            else
            {
                game_music.Stop();
            }
    
        }
        private void KeyIsDown(object sender, KeyEventArgs e)
        {
            if (gameOver == true)
            {
                return;
            }

            if (e.KeyCode == Keys.Left || e.KeyCode == Keys.A)
            {
                goLeft = true;
                facing = "Left";
                player.Image = Properties.Resources.left;
            }

            if (e.KeyCode == Keys.Up || e.KeyCode == Keys.W)
            {
                goUp = true;
                facing = "Up";
                player.Image = Properties.Resources.up;
            }

            if (e.KeyCode == Keys.Down || e.KeyCode == Keys.S)
            {
                goDown = true;
                facing = "Down";
                player.Image = Properties.Resources.down;
            }

            if (e.KeyCode == Keys.Right || e.KeyCode == Keys.D)
            {
                goRight = true;
                facing = "Right";
                player.Image = Properties.Resources.right;
            }

        }

        private void KeyIsUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left || e.KeyCode == Keys.A)
            {
                goLeft = false;
            }

            if (e.KeyCode == Keys.Up || e.KeyCode == Keys.W)
            {
                goUp = false;
            }

            if (e.KeyCode == Keys.Down || e.KeyCode == Keys.S)
            {
                goDown = false;
            }

            if (e.KeyCode == Keys.Right || e.KeyCode == Keys.D)
            {
                goRight = false;
            }
            if (e.KeyCode == Keys.Space && ammo > 0 && gameOver == false)
            {
                ammo--;
                ShootBullet(facing);

                if ( ammo < 1 )
                {
                    DropAmmo();
                }
            }
        }

        private void ShootBullet(string directions)
        {
            Bullet shootBullet = new Bullet();
            shootBullet.directions = directions;
            shootBullet.bulletLeft = player.Left + (player.Width / 2);
            shootBullet.bulletTop = player.Top + (player.Height / 2);
            shootBullet.MakeBullet(this);
        }

        private void MakeZombies()
        {

            PictureBox zombie = new PictureBox();
            zombie.Tag = "zombie";
            zombie.Image = Properties.Resources.zdown;
            zombie.Left = rand.Next(0, 1000);
            zombie.Top = rand.Next(0, 1300);
            zombie.SizeMode = PictureBoxSizeMode.AutoSize;
            zombiesList.Add(zombie);

            this.Controls.Add(zombie);
            player.BringToFront();
        }

        private void DropAmmo()
        {
            PictureBox ammo = new PictureBox();
            ammo.Image = Properties.Resources.ammo_Image;
            ammo.SizeMode = PictureBoxSizeMode.AutoSize;
            ammo.Left = rand.Next(0, this.ClientSize.Width - ammo.Width);
            ammo.Top = rand.Next(0, this.ClientSize.Height - ammo.Height);
            ammo.Tag = "ammo";
            this.Controls.Add(ammo);

            ammo.BringToFront();
            player.BringToFront();

        }

        private void RestartGame()
        {
            player.Image = Properties.Resources.up;

            playSimpleSound2(music);

            foreach (PictureBox i in zombiesList)
            {
                this.Controls.Remove(i);
            }

            zombiesList.Clear();

            for (int i = 0; i < 3; i++)
            {
                MakeZombies();
            }

            goUp = false;
            goLeft = false;
            goDown = false;
            goRight = false;
            gameOver = false;

            playerHealth = 100;
            score = 0;
            ammo = 10;

            GameTimer.Start();
        }

    }
}
