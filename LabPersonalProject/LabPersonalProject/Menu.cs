﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace LabPersonalProject
{
    public partial class Menu : Form
    {
        public bool music = true;


        public Menu()
        {
            InitializeComponent();
            MessageBox.Show("Данный проект использует музыку,для полноценного погружение следует правильно указать пути!", "Внимание");
            playSimpleSound(music);


        }
        private void playSimpleSound(bool check)
        {
            SoundPlayer menu_music = new SoundPlayer();
            menu_music.SoundLocation = "Z:/Study/Mobile/projvisualstudio2/LabPersonalProject/LabPersonalProject/music/menu.wav";
            
            if (check == true)
            {
                menu_music.Play();
            }
            else
            {
                menu_music.Stop();
            }
        }


        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                music = true;
                playSimpleSound(music);
            }
            else
            {
                music = false;
                playSimpleSound(music);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Class1.muscik_check = checkBox1.Checked;
            Form1 newForm = new Form1();
            newForm.Show();
            
        }
    }
}
