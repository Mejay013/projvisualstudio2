﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Windows.Input;

namespace Change_road_lab
{
    public partial class Form1 : Form
    {
        public int color;
        public Form1()
        {
            InitializeComponent();
            
            tableLayoutPanel1.BackColor = Color.White;
            for (int i = 0; i < 121; i++)
            {
                var pb = new PictureBox();
                pb.Name = ("pictureBox" + (i+14).ToString());
                pb.SizeMode = PictureBoxSizeMode.StretchImage;
                pb.BackColor = Color.Green;
                pb.Margin = new Padding(1,1,0,0);
                pb.MouseMove += all_PictureBox1_Click;
                pb.Dock = DockStyle.Fill;
                tableLayoutPanel1.Controls.Add(pb);
                predprosmotr();

            }

            MouseWheel += Form1_MouseWheel;
        }

        private void Form1_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Delta > 0)
            {
                tableLayoutPanel1.Width = tableLayoutPanel1.Width + 20;
                tableLayoutPanel1.Height = tableLayoutPanel1.Height + 20;
            }
            else
            {
                tableLayoutPanel1.Width = tableLayoutPanel1.Width  - 20;
                tableLayoutPanel1.Height = tableLayoutPanel1.Height - 20;
            }
        }

        void save_game()
        {
            using (StreamWriter sw = new StreamWriter("G:/Programming/mobile/projvisualstudio2/Change_road_lab/Change_road_lab/save/save.txt", false, System.Text.Encoding.Default))
            {

                foreach (PictureBox pb in tableLayoutPanel1.Controls)
                {
                if ( pb is PictureBox) 
                    {
                        string name_pb = pb.Name;
                        string location;
                        if (pb.ImageLocation == null)
                        {
                            location = "None";
                        }
                        else
                        {
                            location = pb.ImageLocation;
                        };
                    string text = name_pb + "@" + location + "\n";
                    sw.Write(text);
                    
                    }
                }
            }
        }
        void load_game()
        {
            using (StreamReader sr = new StreamReader("G:/Programming/mobile/projvisualstudio2/Change_road_lab/Change_road_lab/save/save.txt", System.Text.Encoding.Default))
            {
                string line = sr.ReadToEnd();
                string[] lines = line.Split('\n');
                foreach (PictureBox pb in tableLayoutPanel1.Controls)
                {
                    foreach ( string check_line in lines)
                    {
                        string[] values = check_line.Split('@');
                        if (values[0] == pb.Name)
                        {
                            if (values[1] == "None")
                            {
                                continue;
                            }
                            else
                            {
                                pb.SizeMode = PictureBoxSizeMode.StretchImage;
                                pb.ImageLocation = values[1];
                                pb.Image = Image.FromFile(values[1]);
                            }
                        }
                    }
                }
            }
        }
        void predprosmotr()
        {
            string image = check_image();
            pictureBox13.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox13.Image = Image.FromFile(image);
        }
        public string check_image()
        {
            string check;
            if (color == 1){check = "G:/Programming/mobile/projvisualstudio2/Change_road_lab/Change_road_lab/img/1.png";}
            else if (color == 2) { check = "G:/Programming/mobile/projvisualstudio2/Change_road_lab/Change_road_lab/img/6.png"; }
            else if (color == 3) { check = "G:/Programming/mobile/projvisualstudio2/Change_road_lab/Change_road_lab/img/7.png"; }
            else if (color == 4) { check = "G:/Programming/mobile/projvisualstudio2/Change_road_lab/Change_road_lab/img/12.png"; }

            else if (color == 5) { check = "G:/Programming/mobile/projvisualstudio2/Change_road_lab/Change_road_lab/img/2.png"; }
            else if (color == 6) { check = "G:/Programming/mobile/projvisualstudio2/Change_road_lab/Change_road_lab/img/5.png"; }
            else if (color == 7) { check = "G:/Programming/mobile/projvisualstudio2/Change_road_lab/Change_road_lab/img/8.png"; }
            else if (color == 8) { check = "G:/Programming/mobile/projvisualstudio2/Change_road_lab/Change_road_lab/img/11.png"; }

            else if (color == 9)  { check = "G:/Programming/mobile/projvisualstudio2/Change_road_lab/Change_road_lab/img/3.png"; }
            else if (color == 10) { check = "G:/Programming/mobile/projvisualstudio2/Change_road_lab/Change_road_lab/img/4.png"; }
            else if (color == 11) { check = "G:/Programming/mobile/projvisualstudio2/Change_road_lab/Change_road_lab/img/9.png"; }
            else if (color == 12) { check = "G:/Programming/mobile/projvisualstudio2/Change_road_lab/Change_road_lab/img/10.png"; }

            else { check = "G:/Programming/mobile/projvisualstudio2/Change_road_lab/Change_road_lab/img/1.png"; }
            return check;
        }

        private void all_PictureBox1_Click(object sender, EventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {

                string new_image = check_image();
                ((PictureBox)sender).SizeMode = PictureBoxSizeMode.StretchImage;
                ((PictureBox)sender).ImageLocation = new_image;
                ((PictureBox)sender).Image = Image.FromFile(new_image);

            }
        }

        
        private void pictureBox5_Click(object sender, EventArgs e)
        {
            color = 5;
            predprosmotr();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            color = 6;
            predprosmotr();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            color = 7;
            predprosmotr();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            color = 8;
            predprosmotr();
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            color = 9;
            predprosmotr();
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            color = 10;
            predprosmotr();
        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {
            color = 11;
            predprosmotr();
        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            color = 12;
            predprosmotr();
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            color = 1;
            predprosmotr();
        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            color = 2;
            predprosmotr();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            color = 3;
            predprosmotr();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            color = 4;
            predprosmotr();
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            save_game();
            MessageBox.Show("Успех", "Сохранено");
        }

        private void загрузитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            load_game();
        }
    }
}
