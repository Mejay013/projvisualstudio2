﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace labQuequ
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<int> x = new Queue<int>();
            //Queue x = new Queue();
            x.Enqueue(6);
            x.Enqueue(1);
            x.Enqueue(5);
            x.Enqueue(2);
            Console.WriteLine(x.Peek());
            Console.WriteLine("-----");
            while (x.Count > 0)
            {
                Console.WriteLine(x.Dequeue());
            }

        }
    }
}
